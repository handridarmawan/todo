import { Layout } from "antd";
import { Content, Header } from "antd/es/layout/layout";
import { Outlet } from "react-router-dom";

const Backend = () => {
  return (
    <Layout>
      <Header
        style={{
          backgroundColor: "#16ABF8",
          paddingInline: "15%",
          paddingTop: "2%",
          fontSize: "24px",
          height: "105px",
          color: "#FFF",
          fontWeight: "700",
        }}
        data-cy="header-background"
      >
        <span data-cy="header-title">TO DO LIST APP</span>
      </Header>
      <Content style={{ minHeight: "100vh", padding: "5% 15%" }}>
        <Outlet />
      </Content>
    </Layout>
  );
};

export default Backend;

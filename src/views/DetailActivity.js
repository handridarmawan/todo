import pic2 from "../assets/img/todo-empty-state.png";
import { useParams } from "react-router-dom";
import ActivityModule from "../modules/AcitivityModule";
import React, { useEffect, useState } from "react";
import { Col, Row, Input } from "antd";
import {
  Button,
  Dropdown,
  Form,
  FormCheck,
  FormControl,
  FormGroup,
  FormLabel,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "react-bootstrap";
import ButtonSkyshi from "../components/ButtonComponent";
import DropdownToggle from "react-bootstrap/esm/DropdownToggle";
import DropdownMenu from "react-bootstrap/esm/DropdownMenu";
import DropdownItem from "react-bootstrap/esm/DropdownItem";
import Swal from "sweetalert2";
import Select from "react-select";
import { useNavigate } from "react-router-dom";

const DropdownIndicator = (props) => {
  return (
    <div
      data-cy="modal-add-priority-dropdown"
      className="icon-dropdown mr-2"
    ></div>
  );
};

const DetailActivity = () => {
  const { id } = useParams();
  const [dataActivity, setDataActivity] = useState(null);
  const [dataDetail, setDataDetail] = useState({
    data: [],
    total: 0,
  });
  const [editNameActivity, setEditNameActivity] = useState(false);
  const [selectedSort, setSelectedSort] = useState(1);
  const [modalFormDetailActivity, setModalFormDetailActivity] = useState(false);
  const [namaItem, setNamaItem] = useState("");
  const [selectedPriority, setSelectedPriority] = useState(null);
  const [reloadDataDetail, setReloadDataDetail] = useState(false);
  const [dataEditDetailActivity, setdataEditDetailActivity] = useState(null);

  const [dataDetailActivityDelete, setDataDetailActivityDelete] =
    useState(null);
  const [modalDeleteDetailActivity, setModalDetailDeleteActivity] =
    useState(false);
  const [
    modalNotifikasiAfterDeleteDetailActivity,
    setModalNotifikasiAfterDeleteDetailActivity,
  ] = useState(false);

  const navigate = useNavigate();

  const options = [
    {
      value: "very-high",
      label: (
        <div
          data-cy="modal-add-priority-item"
          className="d-flex align-items-center"
        >
          <div className="select-priority very-high"></div>
          <div>Very High</div>
        </div>
      ),
    },
    {
      value: "high",
      label: (
        <div
          data-cy="modal-add-priority-item"
          className="d-flex align-items-center"
        >
          <div className="select-priority high"></div>
          <div>High</div>
        </div>
      ),
    },
    {
      value: "normal",
      label: (
        <div
          data-cy="modal-add-priority-item"
          className="d-flex align-items-center"
        >
          <div className="select-priority normal"></div>
          <div>Medium</div>
        </div>
      ),
    },
    {
      value: "low",
      label: (
        <div
          data-cy="modal-add-priority-item"
          className="d-flex align-items-center"
        >
          <div className="select-priority low"></div>
          <div>Low</div>
        </div>
      ),
    },
    {
      value: "very-low",
      label: (
        <div
          data-cy="modal-add-priority-item"
          className="d-flex align-items-center"
        >
          <div className="select-priority very-low"></div>
          <div>Very Low</div>
        </div>
      ),
    },
  ];

  const fetchActivityMaster = async (id) => {
    return ActivityModule.getActivityById(id).then((result) => {
      setDataActivity(result);
    });
  };

  const fetchActivityDetail = async (id) => {
    return ActivityModule.readDetail(id).then((result) => {
      setDataDetail(result);
    });
  };

  const handleCloseDetailActivity = () => {
    navigate("/");
  };

  const handleChange = (ev) => {
    setDataActivity({ ...dataActivity, title: ev.target.value });
  };

  const handleUpdateActivity = () => {
    const newValues = {
      title: dataActivity.title,
    };

    ActivityModule.updateActivity(dataActivity.id, newValues)
      .then((res) => {})
      .catch((err) => {});

    setEditNameActivity(false);
  };

  const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <button
      className="btn-sort mb-1"
      onClick={(e) => {
        e.preventDefault();
        onClick(e);
      }}
    >
      <div className="icon-sort"></div>
    </button>
  ));

  const CustomMenu = React.forwardRef(
    ({ children, style, className, "aria-labelledby": labeledBy }, ref) => {
      return (
        <div
          ref={ref}
          style={style}
          className={className}
          aria-labelledby={labeledBy}
        >
          <ul className="list-unstyled">{children}</ul>
        </div>
      );
    }
  );

  const handleSortItem = (n) => {
    if (n == 1) {
      dataDetail.data.sort(function (a, b) {
        return b.id - a.id;
      });
    } else if (n == 2) {
      dataDetail.data.sort(function (a, b) {
        return a.id - b.id;
      });
    } else if (n == 3) {
      dataDetail.data.sort(function (a, b) {
        if (b.title > a.title) {
          return -1;
        }
        if (a.title > b.title) {
          return 1;
        }
        return 0;
      });
    } else if (n == 4) {
      dataDetail.data.sort(function (a, b) {
        if (a.title > b.title) {
          return -1;
        }
        if (b.title > a.title) {
          return 1;
        }
        return 0;
      });
    } else if (n == 5) {
      dataDetail.data.sort(function (a, b) {
        return b.is_active - a.is_active;
      });
    }
    setSelectedSort(n);
  };

  const addDetailActivity = () => {
    setModalFormDetailActivity(true);
    setNamaItem("");
    setSelectedPriority({
      value: "very-high",
      label: (
        <div className="d-flex align-items-center">
          <div className="select-priority very-high"></div>
          <div>Very High</div>
        </div>
      ),
    });
  };

  const handleCheckItem = (dt) => {
    const newValues = {
      is_active: dt.is_active === 1 ? 0 : 1,
      priority: dt.priority,
    };

    ActivityModule.updateIsActiveItem(dt.id, newValues)
      .then((res) => {
        setReloadDataDetail(true);
      })
      .catch((err) => {});
  };

  const editDetailActivity = (dt) => {
    setdataEditDetailActivity(dt);
    setModalFormDetailActivity(true);
    setNamaItem(dt.title);
    const dataSelected = options.filter((val) => {
      return val.value == dt.priority;
    });
    setSelectedPriority(dataSelected[0]);
  };

  const handleCloseModalEdit = () => {
    setModalFormDetailActivity(false);
  };

  const handleSubmitItem = () => {
    if (dataEditDetailActivity) {
      const newValues = {
        is_active: dataEditDetailActivity.is_active,
        priority: selectedPriority.value,
        title: namaItem,
      };

      ActivityModule.updateItem(dataEditDetailActivity.id, newValues)
        .then((res) => {
          setdataEditDetailActivity(null);
          setReloadDataDetail(true);
        })
        .catch((err) => {});
    } else {
      const newValues = {
        activity_group_id: dataActivity.id,
        priority: selectedPriority.value,
        title: namaItem,
      };

      ActivityModule.createItem(newValues)
        .then((res) => {
          setReloadDataDetail(true);
        })
        .catch((err) => {});
    }

    setModalFormDetailActivity(false);
  };

  const deleteDetailActivity = (dt) => {
    setDataDetailActivityDelete(dt);
    setModalDetailDeleteActivity(true);
  };

  const handleDeleteDetailActivity = () => {
    ActivityModule.deleteItem(dataDetailActivityDelete.id)
      .then((res) => {
        setDataDetailActivityDelete(null);
        setModalDetailDeleteActivity(false);
        setModalNotifikasiAfterDeleteDetailActivity(true);
        setReloadDataDetail(true);
      })
      .catch((err) => {});
  };

  const handleCloseModalDeleteDetailActivity = () => {
    setModalNotifikasiAfterDeleteDetailActivity(false);
  };

  useEffect(() => {
    fetchActivityMaster(id);
    fetchActivityDetail(id);
    setReloadDataDetail(false);
  }, [id, reloadDataDetail]);

  return (
    <>
      <Row justify="space-between">
        <Col
          span={18}
          style={{
            fontSize: "36px",
            fontWeight: "700",
            display: "flex",
            alignItems: "center",
          }}
        >
          <div
            className="icon-back-button"
            data-cy="todo-back-button"
            onClick={handleCloseDetailActivity}
          ></div>{" "}
          {!editNameActivity ? (
            <span
              style={{ cursor: "text" }}
              onClick={() => {
                setEditNameActivity(true);
              }}
              data-cy="todo-title"
            >
              {dataActivity?.title}
            </span>
          ) : (
            <Input
              bordered={false}
              onChange={(val) => {
                handleChange(val);
              }}
              onBlur={handleUpdateActivity}
              value={dataActivity.title}
              autoFocus
              style={{
                fontSize: "36px",
                fontWeight: "700",
                padding: "0",
                lineHeight: "0",
              }}
            />
          )}
          <div
            className="icon-edit-button"
            data-cy="todo-title-edit-button"
            onClick={() => {
              setEditNameActivity(true);
            }}
          ></div>
        </Col>
        <Col span={5} className="d-flex to-do-sort">
          <Dropdown data-cy="todo-sort-button">
            <DropdownToggle as={CustomToggle}></DropdownToggle>
            <DropdownMenu as={CustomMenu}>
              <DropdownItem
                data-cy="sort-selection"
                onClick={() => {
                  handleSortItem(1);
                }}
              >
                <div
                  className={`d-flex align-items-center ${
                    selectedSort == 1 ? `active` : ``
                  }`}
                >
                  <div className="icon-sort-latest"></div>
                  <span>Terbaru</span>
                </div>
              </DropdownItem>
              <DropdownItem
                data-cy="sort-selection"
                onClick={() => {
                  handleSortItem(2);
                }}
              >
                <div
                  className={`d-flex align-items-center ${
                    selectedSort == 2 ? `active` : ``
                  }`}
                >
                  <div className="icon-sort-oldest"></div>
                  <span>Terlama</span>
                </div>
              </DropdownItem>
              <DropdownItem
                data-cy="sort-selection"
                onClick={() => {
                  handleSortItem(3);
                }}
              >
                <div
                  className={`d-flex align-items-center ${
                    selectedSort == 3 ? `active` : ``
                  }`}
                >
                  <div className="icon-sort-a-z"></div>
                  <span>A-Z</span>
                </div>
              </DropdownItem>
              <DropdownItem
                data-cy="sort-selection"
                onClick={() => {
                  handleSortItem(4);
                }}
              >
                <div
                  className={`d-flex align-items-center ${
                    selectedSort == 4 ? `active` : ``
                  }`}
                >
                  <div className="icon-sort-z-a"></div>
                  <span>Z-A</span>
                </div>
              </DropdownItem>
              <DropdownItem
                data-cy="sort-selection"
                onClick={() => {
                  handleSortItem(5);
                }}
              >
                <div
                  className={`d-flex align-items-center ${
                    selectedSort == 5 ? `active` : ``
                  }`}
                >
                  <div className="icon-sort-unfinished"></div>
                  <span>Belum Selesai</span>
                </div>
              </DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <ButtonSkyshi data-cy="todo-add-button" onClick={addDetailActivity} />
        </Col>
      </Row>
      <Row>
        <Col span={24} style={{ padding: "55px 0" }}>
          {dataDetail.total == 0 ? (
            <div
              style={{ textAlign: "center" }}
              onClick={addDetailActivity}
              data-cy="todo-empty-state"
            >
              <img src={pic2} onClick={() => {}} alt="empty activity" />
            </div>
          ) : (
            Object.keys(dataDetail.data).map((key) => {
              return (
                <div className="to-do-item d-flex justify-content-between align-items-center mb-3">
                  <div className="d-flex align-items-center">
                    <FormCheck
                      type="checkbox"
                      className="checkbox-item"
                      onChange={() => {
                        handleCheckItem(dataDetail.data[key]);
                      }}
                      checked={
                        dataDetail.data[key].is_active == 1 ? false : true
                      }
                      data-cy="todo-item-checkbox"
                    />
                    <div
                      data-cy="todo-item-priority-indicator"
                      className={`select-priority ${dataDetail.data[key].priority}`}
                    ></div>
                    <span
                      style={{
                        fontSize: "18px",
                        fontWeight: "500",
                        lineHeight: "27px",
                      }}
                      className={
                        dataDetail.data[key].is_active == 1
                          ? ``
                          : `item-deleted`
                      }
                      data-cy="todo-item-title"
                    >
                      {dataDetail.data[key].title}
                    </span>
                    <div
                      className="icon-edit-button"
                      onClick={() => {
                        editDetailActivity(dataDetail.data[key]);
                      }}
                      data-cy="todo-item-edit-button"
                    ></div>
                  </div>
                  <div>
                    <div
                      className="icon-delete"
                      onClick={(ev) => {
                        deleteDetailActivity(dataDetail.data[key]);
                      }}
                      data-cy="todo-item-delete-button"
                    ></div>
                  </div>
                </div>
              );
            })
          )}
        </Col>
      </Row>
      <Modal
        data-cy="modal-add"
        show={modalFormDetailActivity}
        onHide={handleCloseModalEdit}
      >
        <ModalHeader>
          <ModalTitle data-cy="modal-add-title">Tambah List Item</ModalTitle>
          <button
            data-cy="modal-add-close-button"
            type="button"
            className="btn-close"
            aria-label="Close"
            onClick={handleCloseModalEdit}
          ></button>
        </ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup className="mb-3">
              <FormLabel data-cy="modal-add-name-title">
                Nama List Item
              </FormLabel>
              <FormControl
                data-cy="modal-add-name-input"
                type="text"
                placeholder="Tambahkan Nama Activity"
                value={namaItem}
                onChange={(val) => {
                  setNamaItem(val.target.value);
                }}
              />
            </FormGroup>
            <FormGroup>
              <FormLabel data-cy="modal-add-priority-title">Priority</FormLabel>
              <Select
                components={{ DropdownIndicator }}
                options={options}
                defaultValue={selectedPriority}
                onChange={(val) => {
                  setSelectedPriority(val);
                }}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button
            data-cy="modal-add-save-button"
            disabled={namaItem ? false : true}
            variant="primary"
            onClick={handleSubmitItem}
          >
            Simpan
          </Button>
        </ModalFooter>
      </Modal>

      <Modal
        data-cy="modal-delete"
        show={modalDeleteDetailActivity}
        onHide={handleCloseModalDeleteDetailActivity}
      >
        <ModalHeader
          className="justify-content-center"
          style={{ borderBottom: 0 }}
        >
          <ModalTitle>
            <div
              data-cy="modal-delete-icon"
              className="icon-delete-modal"
            ></div>
          </ModalTitle>
        </ModalHeader>
        <ModalBody className="text-center">
          <div data-cy="modal-delete-title">
            Apakah anda yakin menghapus item{" "}
            <strong>{dataDetailActivityDelete?.title}</strong>?{" "}
          </div>
        </ModalBody>
        <ModalFooter
          style={{ borderTop: 0 }}
          className="justify-content-center"
        >
          <Button
            data-cy="modal-delete-confirm-button"
            variant="danger"
            onClick={handleDeleteDetailActivity}
          >
            Hapus
          </Button>
          <Button
            data-cy="modal-delete-cancel-button"
            variant="primary"
            onClick={() => {
              setModalDetailDeleteActivity(false);
            }}
          >
            Batal
          </Button>
        </ModalFooter>
      </Modal>
      <Modal
        show={modalNotifikasiAfterDeleteDetailActivity}
        data-cy="modal-information"
        onHide={handleCloseModalDeleteDetailActivity}
      >
        <ModalBody className="d-flex">
          <div
            data-cy="modal-information-icon"
            className="information-icon"
          ></div>
          <div data-cy="modal-information-title">Item berhasil dihapus</div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default DetailActivity;

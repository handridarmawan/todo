import { Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import pic1 from "../assets/img/1.png";
import ButtonSkyshi from "../components/ButtonComponent";
import ActivityModule from "../modules/AcitivityModule";
import moment from "moment";
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import { useNavigate } from "react-router-dom";

const Activity = () => {
  const [data, setData] = useState({
    data: [],
    total: 0,
  });
  const [dataActivityDelete, setDataActivityDelete] = useState(null);
  const [loadingButton, setLoadingButton] = useState(false);
  const [reloadData, setReloadData] = useState(false);
  const [modalDeleteActivity, setModalDeleteActivity] = useState(false);
  const [
    modalNotifikasiAfterDeleteActivity,
    setModalNotifikasiAfterDeleteActivity,
  ] = useState(false);

  const navigate = useNavigate();

  const handleCloseModalDeleteActivity = () => {
    setModalDeleteActivity(false);
  };

  const handleCloseModalNotifikasiAfterDeleteActivity = () => {
    setModalNotifikasiAfterDeleteActivity(false);
  };

  const fetchActivity = async () => {
    return ActivityModule.read().then((result) => {
      setData(result);
    });
  };

  const addActivity = () => {
    setLoadingButton(true);
    ActivityModule.create()
      .then((res) => {
        setLoadingButton(false);
        setReloadData(true);
      })
      .catch((err) => {});
  };

  const deleteActivity = (dt) => {
    setDataActivityDelete(dt);
    setModalDeleteActivity(true);
  };

  const handleDeleteActivity = () => {
    ActivityModule.deleteActivity(dataActivityDelete.id)
      .then((res) => {
        setDataActivityDelete(null);
        setModalDeleteActivity(false);
        setModalNotifikasiAfterDeleteActivity(true);
        setReloadData(true);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    fetchActivity();
    setReloadData(false);
  }, [reloadData]);

  return (
    <div>
      <>
        <Row justify="space-between">
          <Col
            span={4}
            style={{ fontSize: "36px", fontWeight: "700" }}
            data-cy="activity-title"
          >
            Activity
          </Col>
          <Col span={3}>
            <ButtonSkyshi
              onClick={addActivity}
              loading={loadingButton}
              data-cy="activity-add-button"
            />
          </Col>
        </Row>
        <Row>
          <Col span={23} style={{ padding: "55px 0" }}>
            {data.total == 0 ? (
              <div
                style={{ textAlign: "center" }}
                onClick={addActivity}
                data-cy="activity-empty-state"
              >
                <img src={pic1} onClick={() => {}} alt="empty activity" />
              </div>
            ) : (
              <Row
                gutter={
                  ({
                    xs: 8,
                    sm: 16,
                    md: 24,
                    lg: 32,
                  },
                  [54, 16])
                }
              >
                {Object.keys(data.data).map((key) => {
                  return (
                    <Col className="gutter-row" span={6}>
                      <div
                        style={{
                          background: "#FFFFFF",
                          borderRadius: "12px",
                          boxShadow: "0px 6px 10px #0000001a",
                          padding: "22px 27px",
                          width: "235px",
                          height: "234px",
                        }}
                      >
                        <div
                          style={{
                            display: "flex",
                            flexWrap: "wrap",
                            height: "100%",
                            alignContent: "space-between",
                          }}
                          data-cy="activity-item"
                        >
                          <div
                            style={{ height: "158px", cursor: "pointer" }}
                            onClick={() => {
                              navigate("/detail/" + data.data[key].id);
                            }}
                          >
                            <div
                              style={{
                                width: "100%",
                                fontWeight: "700",
                                fontSize: "18px",
                                lineHeight: "27px",
                              }}
                              data-cy="activity-item-title"
                            >
                              {data.data[key].title}
                            </div>
                          </div>
                          <div
                            style={{
                              width: "100%",
                              display: "flex",
                              justifyContent: "space-between",
                              zIndex: "100",
                            }}
                          >
                            <div
                              data-cy="activity-item-date"
                              style={{
                                fontSize: "16px",
                                lineHeight: "21px",
                                color: "#888888",
                                alignItems: "center",
                              }}
                            >
                              {moment(data.data[key].created_at)
                                .format("DD MMMM YYYY")
                                .toLocaleString()}
                            </div>
                            <div
                              data-cy="activity-item-delete-button"
                              className="icon-delete"
                              onClick={(ev) => {
                                ev.stopPropagation();
                                deleteActivity(data.data[key]);
                              }}
                            ></div>
                          </div>
                        </div>
                      </div>
                    </Col>
                  );
                })}
              </Row>
            )}
          </Col>
        </Row>
        <Modal
          data-cy="modal-delete"
          show={modalDeleteActivity}
          onHide={handleCloseModalDeleteActivity}
        >
          <ModalHeader
            className="justify-content-center"
            style={{ borderBottom: 0 }}
          >
            <ModalTitle>
              <div
                data-cy="modal-delete-icon"
                className="icon-delete-modal"
              ></div>
            </ModalTitle>
          </ModalHeader>
          <ModalBody className="text-center">
            <div data-cy="modal-delete-title">
              Apakah anda yakin menghapus activity{" "}
              <strong>{dataActivityDelete?.title}</strong>?{" "}
            </div>
          </ModalBody>
          <ModalFooter
            style={{ borderTop: 0 }}
            className="justify-content-center"
          >
            <Button
              data-cy="modal-delete-confirm-button"
              variant="danger"
              onClick={handleDeleteActivity}
            >
              Hapus
            </Button>
            <Button
              data-cy="modal-delete-cancel-button"
              variant="primary"
              onClick={() => {
                setModalDeleteActivity(false);
              }}
            >
              Batal
            </Button>
          </ModalFooter>
        </Modal>
        <Modal
          show={modalNotifikasiAfterDeleteActivity}
          data-cy="modal-information"
          onHide={handleCloseModalNotifikasiAfterDeleteActivity}
        >
          <ModalBody className="d-flex">
            <div
              data-cy="modal-information-icon"
              className="information-icon"
            ></div>
            <div data-cy="modal-information-title">
              Activity berhasil dihapus
            </div>
          </ModalBody>
        </Modal>
      </>
    </div>
  );
};

export default Activity;

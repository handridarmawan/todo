import { PlusOutlined } from "@ant-design/icons";
import { Button } from "antd";

const ButtonSkyshi = ({ ...props }) => {
  return (
    <Button
      type="primary"
      size="large"
      icon={<PlusOutlined />}
      shape="round"
      {...props}
      style={{ backgroundColor: "#16ABF8" }}
    >
      Tambah
    </Button>
  );
};

export default ButtonSkyshi;

import axios from "axios";

const BASE_API_URL = process.env.REACT_APP_API;
const EMAIL = process.env.REACT_APP_EMAIL;

const read = () => {
  return axios.get(`${BASE_API_URL}activity-groups?email=${EMAIL}`);
};

const readDetail = (id) => {
  return axios.get(`${BASE_API_URL}todo-items?activity_group_id=${id}`);
};

const getActivityById = (id) => {
  return axios.get(`${BASE_API_URL}activity-groups/${id}`);
};

const create = () => {
  return axios.post(`${BASE_API_URL}activity-groups`, {
    title: "New Activity",
    email: `${EMAIL}`,
  });
};

const createItem = (payload) => {
  return axios.post(`${BASE_API_URL}todo-items`, payload);
};

const updateIsActiveItem = (id, payload) => {
  return axios.patch(`${BASE_API_URL}todo-items/${id}`, payload);
};

const updateItem = (id, payload) => {
  return axios.patch(`${BASE_API_URL}todo-items/${id}`, payload);
};

const updateActivity = (id, payload) => {
  return axios.patch(`${BASE_API_URL}activity-groups/${id}`, payload);
};

const deleteItem = (id) => {
  return axios.delete(`${BASE_API_URL}todo-items/${id}`);
};

const deleteActivity = (id) => {
  return axios.delete(`${BASE_API_URL}activity-groups/${id}`);
};

export default {
  read,
  create,
  readDetail,
  createItem,
  updateIsActiveItem,
  updateItem,
  deleteItem,
  deleteActivity,
  updateActivity,
  getActivityById,
};

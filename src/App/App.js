import { Route, Routes } from "react-router-dom";
import Activity from "../views/Activity";
import Backend from "../layout/Backend";
import DetailActivity from "../views/DetailActivity";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Backend />}>
          <Route index element={<Activity />} />
          <Route path="/detail/:id" element={<DetailActivity />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
